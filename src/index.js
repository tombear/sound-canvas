import { SoundCanvas } from "./main";

if (customElements && customElements.get("sound-canvas") === undefined)
  customElements.define("sound-canvas", SoundCanvas);

export { SoundCanvas } from "./main";