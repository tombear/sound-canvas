import { switchMap, tap } from "rxjs/operators";
import H from "callbag-html";
import Spline from "cubic-spline";
import { Observable } from "rxjs";

export const makePoint = ({ x, y }) => {
  let path = new Path2D();
  path.arc(x, y, 5, 0, Math.PI * 180);
  return { x, y, path };
}


export const insertPoint = (oldPoints, newPoint) => {
  const newPoints = oldPoints.filter(p => Math.abs(newPoint.x - p.x) > 10);
  return [...newPoints, newPoint].sort((a, b) => a.x - b.x);
}

export const popAtIndex = (xs, i) => {
  if (i < 0 || xs[i] === undefined) return [...xs];
  return [...xs.slice(0, i), ...xs.slice(i + 1)];
}

export const indexInsertedPoint = (points, { x, y }) => {
  const newPoint = makePoint({ x, y });
  const newPoints = [...points, newPoint].sort((a, b) => a.x - b.x);
  const pointer = newPoints.indexOf(newPoint);
  return { points: newPoints, pointer };
}

export const addMovementToStateWithPointer = ({ state, pointer }) => ({ movementX, movementY }) => {
  let point = { ...state.points[pointer] };
  let points = popAtIndex([...state.points], pointer);
  point.x += movementX;
  point.y += movementY;
  let { points: newPoints, pointer: newPointer } = indexInsertedPoint(points, point);
  return { state: { ...state, points: newPoints }, pointer: newPointer };
}

export function toObservable(source) {
  return new Observable(observer =>  {
    let talkback;
    try {
      source(0, (t, d) => {
        if (t === 0) talkback = d;
        if (t === 1) observer.next(d);
        if (t === 2 && d) observer.error(d);
        else if (t === 2) talkback = void 0, observer.complete(d);
      });
    } catch (err) {
      observer.error(err);
    }
    return function unsubscribe() {
      if (talkback) talkback(2);
    };
  });
}

class SwitchScanOperator {
  constructor(accumulator, seed) {
    this.accumulator = accumulator;
    this.seed = seed;
  }
  call(subscriber, source) {
    let seed = this.seed;
    return source.pipe(switchMap((value, index) => this.accumulator(seed, value, index)), tap((value) => seed = value)).subscribe(subscriber);
  }
}

export function switchScan(accumulator, seed) {
  return (source) => source.lift(new SwitchScanOperator(accumulator, seed));
}