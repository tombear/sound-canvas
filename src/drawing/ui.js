import H from "callbag-html";

export const drawControls = ({ controls, drawMode, emitter }) => {
  const drawModes =
    ["free", "bezier", "erase"]
      .map(lbl =>
        H.div([
          H.input({
            type: "radio",
            value: lbl,
            checked: lbl === drawMode ? true : false
          }),
          H.label(lbl)
        ]));

  const drawModeContainer = H.div({
    onchange: ev => emitter({ drawMode: ev.target.value })
  }, drawModes);

  const submitButton = H.button({ type: "submit", onclick: () => emitter({ action: "loadWaveForm" })}, "load waveform" )

  controls.innerHTML = "";
  controls.appendChild(drawModeContainer);
  controls.appendChild(submitButton)
  // drawModes.forEach(c => controls.appendChild(c));
}

export const drawCanvas = state => {
  let { context, points } = { ...state };
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
  points.forEach(({ path }) => context.stroke(path));
  context.beginPath();
  context.moveTo(0, context.canvas.height / 2);
  function catmullRom(points, ctx) {

    var p0, p1, p2, p3, i6 = 1.0 / 6.0;

    for (var i = 3, n = points.length; i < n; i++) {

      p0 = points[i - 3];
      p1 = points[i - 2];
      p2 = points[i - 1];
      p3 = points[i];

      ctx.bezierCurveTo(
        p2.x * i6 + p1.x - p0.x * i6,
        p2.y * i6 + p1.y - p0.y * i6,
        p3.x * -i6 + p2.x + p1.x * i6,
        p3.y * -i6 + p2.y + p1.y * i6,
        p2.x,
        p2.y
      );
    }
  }

  points = [{ x: 0, y: context.canvas.height / 2 }, ...points, { x: context.canvas.width, y: context.canvas.height / 2 }]
  catmullRom(points, context);
  context.lineTo(context.canvas.width, context.canvas.height / 2);
  context.stroke();
  context.beginPath();

  if (points.length > 8) {
    // const ev = new CustomEvent("SOUND", { detail: { points, w: context.canvas.width, h: context.canvas.height }, bubbles: true, composed: true });
    // context.canvas.dispatchEvent(ev);
  }
};
