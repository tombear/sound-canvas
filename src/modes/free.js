import { fromEvent } from "rxjs";
import { flatMap, startWith, takeUntil, throttleTime, map, scan } from "rxjs/operators";
import { insertPoint, makePoint } from "../util";

export const freeMode = state => {

  const canvasOn = ev => fromEvent(state.context.canvas, ev);

  return canvasOn("mousedown").pipe(
    flatMap(downEvent => canvasOn("mousemove").pipe(
      startWith(downEvent),
      takeUntil(canvasOn("mouseup")),
    )),
    throttleTime(state.freeHandSpeed),
    map(({ offsetX, offsetY }) => ({ x: offsetX, y: offsetY })),
    scan((acc, { x, y }) => ({
      ...acc,
      points: insertPoint(acc.points, makePoint({ x, y }, acc.pointMode))
    }), { ...state })
  );
}