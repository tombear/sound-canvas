import { fromEvent, of } from "rxjs";
import { scan, switchMap, tap, map, mergeMap, switchMapTo, mergeMapTo, takeUntil, mapTo } from "rxjs/operators";
import { switchScan } from "../util";

const drawRect = state => {
  state.context.re
}

export const eraseMode = state => {

  const canvasOn = ev => fromEvent(state.context.canvas, ev);

  return canvasOn("mousedown").pipe(
    map(({ offsetX: x, offsetY: y }) => ({
      ...state,
      eraseRect: {
        from: { x, y },
        to: { x, y }
      }
    })),
    switchMap(state => canvasOn("mousemove").pipe(
      scan((acc, { movementX: dx, movementY: dy }) => {
        return {
          ...acc,
          eraseRect: {
            ...acc.eraseRect,
            to: {
              x: acc.eraseRect.to.x + dx,
              y: acc.eraseRect.to.y + dy
            }
          }
        }
      }, state),
      takeUntil(canvasOn("mouseup"))
    )),
    mergeMap(state => canvasOn("mouseup").pipe(
      map(_ => {
        const eraseRectFinal = { ...state.eraseRect };
        delete state.eraseRect;
        return {
          ...state,
          eraseRectFinal
        }
      })
    ))
  );
}