import { makePoint, indexInsertedPoint, addMovementToStateWithPointer, switchScan } from "../util";
import { fromEvent, of } from "rxjs";
import { scan, startWith, takeUntil, map, switchMap } from "rxjs/operators";

export const bezierMode = state => {

  const canvasOn = ev => fromEvent(state.context.canvas, ev);

  return canvasOn("mousedown").pipe(
    switchScan((acc, downEvent) => {
      let { points, pointMode, context, ...rest } = acc;
      let { offsetX: x, offsetY: y } = downEvent
      let pointer = acc.points.findIndex(({ path }) => context.isPointInPath(path, x, y));
      let seed = null;
      if (pointer === -1) {
        let newPoint = makePoint({ x, y }, pointMode);
        let { points: newPoints, pointer } = indexInsertedPoint(points, newPoint);
        seed = { state: { points: newPoints, context, pointMode, ...rest }, pointer };
      } else {
        seed = { state: { points, context, pointMode, ...rest }, pointer };
      }

      return of(seed).pipe(
        switchMap((seed) =>
          canvasOn("mousemove").pipe(
            scan((stateWithPointer, moveEvent) => {
              let X = addMovementToStateWithPointer(stateWithPointer)(moveEvent);
              return X;
            }, seed),
            startWith(seed),
            takeUntil(canvasOn("mouseup")),
          )
        ),
        map(({ state }) => state)
      );
    }, state));
}