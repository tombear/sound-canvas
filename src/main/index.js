import fromFunction from "callbag-from-function";
import H from "callbag-html";
import { of } from "rxjs";
import { map, startWith, switchMap, tap } from "rxjs/operators";
import { drawCanvas, drawControls } from "../drawing";
import { bezierMode, eraseMode, freeMode } from "../modes";
import { switchScan, toObservable } from "../util";

export class SoundCanvas extends HTMLElement {
  static get observedAttributes() {
    return ["canvas-width", "canvas-height"];
  }
  get canvasWidth() {
    let w = this.getAttribute("canvas-width");
    return w ? parseInt(w) : 300
  }
  set canvasWidth(v) {
    this.canvasWidth = v;
  }
  get canvasHeight() {
    let h = this.getAttribute("canvas-height");
    return h ? parseInt(h) : 150
  }
  set canvasHeight(v) {
    this.canvasHeight = v;
  }

  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    canvas.style.border = "1px solid black";
    canvas.width = this.canvasWidth;
    canvas.height = this.canvasHeight;
    shadowRoot.appendChild(canvas);

    const { source, emitter } = fromFunction();
    const source$ = toObservable(source);

    const controls = H.div({
      id: "controls",
      // onchange: ev => emitter({ drawMode: ev.target.value })
    });
    shadowRoot.appendChild(controls);

    const initialState = {
      controls,
      context,
      emitter,
      drawMode: "free",
      freeHandSpeed: 50,
      points: [],
    }

    const canvas$ = source$.pipe(
      startWith(initialState),
      switchScan((acc, v) => of({ ...acc, ...v }).pipe(
        tap(drawControls),
        map(state => {
          if (state.action === "play") {
            delete state.action;
            state.context.canvas.dispatchEvent(new CustomEvent("SOUND", {
              detail: {
                points: state.points,
                w: state.context.canvas.width,
                h: state.context.canvas.height
              },
              bubbles: true,
              composed: true
            }))
          }
          return { ...state };
        }),
        switchMap(state => ({
          "free": freeMode(state),
          "bezier": bezierMode(state),
          "erase": eraseMode(state)
        })[state.drawMode]),
      ), initialState),
    );

    canvas$.subscribe(drawCanvas);
  }
}